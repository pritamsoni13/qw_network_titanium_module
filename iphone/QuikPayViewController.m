//
//  FooViewController.m
//  QWConsumer
//
//  Created by Deep on 24/04/17.
//
//

#import "QuikPayViewController.h"
#import "QWConsumerConstants.h"
#import "QWActivityHelper.h"
#import "QWPayNowViewController.h"

@interface QuikPayViewController ()

@end

NSString * MODULE_PATH;

@implementation QuikPayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    MODULE_PATH = [NSString stringWithFormat:@"modules/%@/", MODULE_ID];
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: MODULE_PATH];
    NSBundle *bundle = [NSBundle bundleWithPath:path];
    
    self = [super initWithNibName:@"QuikPayViewController" bundle: bundle];
    
    if (self) {
        [self commonSetup];
    }
    return self;
}

-(void) popBack {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void) commonSetup{
    self.merchantId.delegate = self;
    
    [self registerForKeyboardNotifications];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:(UIBarButtonSystemItemStop)
                                    target:self
                                    action:@selector(popBack)];
    closeButton.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = closeButton;
    self.navigationItem.title = @"Scan to Pay";
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    self.merchantId.autocorrectionType = UITextAutocorrectionTypeNo;
    self.merchantId.inputAccessoryView = keyboardDoneButtonView;
    
    self.isReading = NO;
    self.captureSession = nil;
    self.isCaptured = NO;

}

#pragma mark Form Validation
- (NSString *)validateForm {
    if (![self.merchantId hasText]) {
         return @"Please enter a valid Merchant code";
    }

    return NULL;
}

- (IBAction)quikPay:(id)sender {
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSString *errorMessage = [self validateForm];
    
    if(errorMessage)
    {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        if ([UIAlertController class]) {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:errorMessage
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message: errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        [self.data setObject:self.merchantId.text forKey:@"merchantid"];
        
        QWPayNowViewController *payNow = [[QWPayNowViewController alloc]initWithNibName:@"QWPayNowViewController" bundle:nil];
        payNow.data = self.data;
        [self.navigationController pushViewController:payNow animated:YES];
    }
}

#pragma mark QRPay Implementation
- (IBAction)startReadingQRCode:(id)sender {
    if (!self.isReading) {
        
        if ([self startReading]) {
            self.startBtn.hidden = YES;
        }
        
    }else{
        [self stopReading];
        self.startBtn.hidden = NO;
    }
    
    self.isReading = !self.isReading;
    _isCaptured = NO;
}

- (BOOL)startReading {
    NSError *error;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    
    // Start video capture.
    [_captureSession startRunning];
    
    return YES;
}

-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    _captureSession = nil;
    _isReading = NO;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [self.videoPreviewLayer removeFromSuperlayer];
    self.startBtn.hidden= NO;
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    if (!_isCaptured) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [QWActivityHelper displayActivityIndicator:self.view];
        });
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if (metadataObjects != nil && [metadataObjects count] > 0) {
            // Get the metadata object.
            AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
            if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
                NSLog(@"QR CODE %@", [metadataObj stringValue]);
                _isCaptured = YES;
                
                [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                
                [QWActivityHelper removeActivityIndicator:self.view];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"Sorry, we can not identify the merchant with this qr code. lease try entering the merchant code below."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if ([self.merchantId isFirstResponder]) {
        
        NSDictionary* info = [aNotification userInfo];
        CGRect keyPadFrame=[[UIApplication sharedApplication].keyWindow convertRect:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue] fromView:self.view];
        CGSize kbSize =keyPadFrame.size;
        
        CGRect activeRect=[self.view convertRect:self.merchantId.frame fromView:self.merchantId.superview];
        CGRect aRect = self.view.bounds;
        aRect.size.height -= (kbSize.height);
        
        CGPoint origin =  activeRect.origin;
        origin.y -= self.scrollView.contentOffset.y;
        
        if (!CGRectContainsPoint(aRect, origin)) {
            CGPoint scrollPoint = CGPointMake(0.0,CGRectGetMaxY(activeRect)-(aRect.size.height));
            [self.scrollView setContentOffset:scrollPoint animated:NO];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    CGRect frame = self.scrollView.frame;
    self.scrollView.frame = frame;
}


@end
