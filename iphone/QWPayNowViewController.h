//
//  QWPayNowViewController.h
//  QWConsumer
//
//  Created by Deep on 25/04/17.
//
//

#import <UIKit/UIKit.h>

@interface QWPayNowViewController : UIViewController <UITextFieldDelegate>
@property (nonatomic, strong) NSMutableDictionary *data;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *amountField;
@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *scrollView;
@end
