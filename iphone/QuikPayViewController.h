//
//  FooViewController.h
//  QWConsumer
//
//  Created by Deep on 24/04/17.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface QuikPayViewController : UIViewController<AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate>

@property (nonatomic) BOOL isReading;
@property (nonatomic) BOOL isCaptured;
@property (nonatomic, strong) NSMutableDictionary *data;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

- (IBAction)quikPay:(id)sender;
- (IBAction)startReadingQRCode:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (weak, nonatomic) IBOutlet UITextField *merchantId;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *scrollView;

@end
