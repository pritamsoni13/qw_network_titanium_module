/**
 * QWNetwork
 *
 * Created by Your Name
 * Copyright (c) 2017 Your Company. All rights reserved.
 */

#import "ComLivquikQwnwModule.h"
#import "TiBase.h"
#import "TiHost.h"
#import "TiUtils.h"
#import "TiApp.h"
#import "QuikPayViewController.h"

@implementation ComLivquikQwnwModule

#pragma mark Internal

// this is generated for your module, please do not change it
-(id)moduleGUID
{
	return @"c00254aa-caaf-4c6e-b46e-0e08322d3154";
}

// this is generated for your module, please do not change it
-(NSString*)moduleId
{
	return @"com.livquik.qwnw";
}

#pragma mark Lifecycle

-(void)startup
{
	// this method is called when the module is first loaded
	// you *must* call the superclass
	[super startup];

	NSLog(@"[INFO] %@ loaded",self);
}

-(void)shutdown:(id)sender
{
	// this method is called when the module is being unloaded
	// typically this is during shutdown. make sure you don't do too
	// much processing here or the app will be quit forceably

	// you *must* call the superclass
	[super shutdown:sender];
}

#pragma mark Cleanup

-(void)dealloc
{
	// release any resources that have been retained by the module
	[super dealloc];
}

#pragma mark Internal Memory Management

-(void)didReceiveMemoryWarning:(NSNotification*)notification
{
	// optionally release any resources that can be dynamically
	// reloaded once memory is available - such as caches
	[super didReceiveMemoryWarning:notification];
}

#pragma mark Listener Notifications

-(void)_listenerAdded:(NSString *)type count:(int)count
{
	if (count == 1 && [type isEqualToString:@"my_event"])
	{
		// the first (of potentially many) listener is being added
		// for event named 'my_event'
	}
}

-(void)_listenerRemoved:(NSString *)type count:(int)count
{
	if (count == 0 && [type isEqualToString:@"my_event"])
	{
		// the last listener called for event named 'my_event' has
		// been removed, we can optionally clean up any resources
		// since no body is listening at this point for that event
	}
}

#pragma Public APIs

-(void)init:(id)args
{
    QuikPayViewController *quikay = [[QuikPayViewController alloc]initWithNibName:@"QuikPayViewController" bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:quikay];
    
    nav.navigationBar.barTintColor = [UIColor colorWithRed:0.89 green:0.00 blue:0.10 alpha:1.0];
    nav.navigationBar.tintColor = [UIColor whiteColor];
    [nav.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    nav.navigationBar.translucent = NO;
    // [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[nav navigationBar] setBarStyle:UIStatusBarStyleLightContent];
    
    [[TiApp controller] presentViewController:nav animated:YES completion:^{
        
    }];
}

@end
