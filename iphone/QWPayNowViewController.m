//
//  QWPayNowViewController.m
//  QWConsumer
//
//  Created by Deep on 25/04/17.
//
//

#import "QWPayNowViewController.h"
#import "QWConsumerConstants.h"
#import "QWActivityHelper.h"

@interface QWPayNowViewController ()

@end

NSString * MODULE_PATH;

@implementation QWPayNowViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    MODULE_PATH = [NSString stringWithFormat:@"modules/%@/", MODULE_ID];
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: MODULE_PATH];
    NSBundle *bundle = [NSBundle bundleWithPath:path];
    
    self = [super initWithNibName:@"QWPayNowViewController" bundle: bundle];
    
    if (self) {
        [self commonSetup];
    }
    return self;
}

-(void) popBack {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void) commonSetup{
    self.amountField.delegate = self;
    [self registerForKeyboardNotifications];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    
    self.navigationItem.title = @"Payment Details";
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    self.amountField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.amountField.inputAccessoryView = keyboardDoneButtonView;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if ([self.amountField isFirstResponder]) {
        
        NSDictionary* info = [aNotification userInfo];
        CGRect keyPadFrame=[[UIApplication sharedApplication].keyWindow convertRect:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue] fromView:self.view];
        CGSize kbSize =keyPadFrame.size;
        
        CGRect activeRect=[self.view convertRect:self.amountField.frame fromView:self.amountField.superview];
        CGRect aRect = self.view.bounds;
        aRect.size.height -= (kbSize.height);
        
        CGPoint origin =  activeRect.origin;
        origin.y -= self.scrollView.contentOffset.y;
        
        if (!CGRectContainsPoint(aRect, origin)) {
            CGPoint scrollPoint = CGPointMake(0.0,CGRectGetMaxY(activeRect)-(aRect.size.height));
            [self.scrollView setContentOffset:scrollPoint animated:NO];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    CGRect frame = self.scrollView.frame;
    self.scrollView.frame = frame;
}


@end
