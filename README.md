### QWCNetwork Titanium module
[![Quikwallet](http://quikwallet.com/assets/images/logo.png)](http://quikwallet.com)
#### Installation
- First clone the module
```$ git clone https://bitbucket.org/livquik/qw_network_titanium_module.git ```
- In Titanium Studio, click on ```Help --> Install Mobile Module``` and Select the Zip file inside ```dist``` folder of the specific platform.
- Choose Output Location as ```Titanium SDK``` and click OK.
### Getting Started
Edit the modules section of your ```tiapp.xml``` file to include this module:
```xml
<modules>
    <module platform="android">com.livquik.qwnw</module>
    <module platform="iphone">com.livquik.qwnw</module>
</modules>
```
On the iOS platform, add the following property to the ```<ios><plist><dict>``` section in ```tiapp.xml```
```
<key>NSCameraUsageDescription</key>
<string>Need camera access for scanning QR Code</string>
```
Now edit ```app.js``` and add the following lines 
```js
var qwnetwork = require('com.livquik.qwnw');

var btn = Titanium.UI.createButton({
	title : 'Load QWNetwork Sdk',
	bottom : 0, left : 0,height : 35, width : '100%',
	backgroundColor: "#e2001a", color : "#ffffff",
	textalign: 'center'
});
$.index.add(btn);

btn.addEventListener('click', function(e) {
	var data = {"partnerid": "XXXXX", "secret": "XXXXXX"};
	qwnetwork.init(data);
});
$.index.open();
```
From here, you can hit the Run button and you’ll see the simulator pop up with the welcome screen.
### Still need help?
We’re here to help. Get in touch and we’ll get back to you as soon as we can. [Contact us](mailto:support@livquik.com)


